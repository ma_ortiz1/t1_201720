package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {



	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}


	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
		int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}

		}
		return max;
	}
	public double getMin(IntegersBag bag)
	{
		double min = Integer.MAX_VALUE;
		int value;
		if (bag !=null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();				
				if (min>value){
					min=value;
				}

			}
		}
		return  min;
	}
	public int getSum (IntegersBag bag){
		int sum=0;		
		if(bag != null){
			Iterator <Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				sum +=iter.next();
			}
		}
		return sum;

	}
	public double getEvenNumbers(IntegersBag bag){
		int numofeven=0;
		int value;
		if(bag != null){
			Iterator <Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value=iter.next();
				if(value%2 == 0){
					numofeven++;				 
				}
			}
		}
		return numofeven;
	}
	public double getOddNumbers(IntegersBag bag){
		int numofodd=0;
		int value;
		if(bag != null){
			Iterator <Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value=iter.next();
				if(value%2 !=0){
					numofodd++;				 
				}
			}
		}
		return numofodd;


	}
}
