package model.data_structures;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;


public class IntegersBag {

	private HashSet<Integer> bag;

	public IntegersBag(){
		this.bag = new HashSet<Integer>();
	}

	public IntegersBag(ArrayList<Integer> data){
		this();
		if(data != null){
			for (Integer datum : data) {
				bag.add(datum);
			}
		}

	}


	public void addDatum(Integer datum){
		bag.add(datum);
	}

	public Iterator<Integer> getIterator(){
		return this.bag.iterator();
	}

}
